# Generated by Django 3.1.3 on 2021-02-03 19:57

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TransactionUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=200, unique=True, validators=[django.core.validators.RegexValidator('^[0-9a-zA-Z./\\\\]*$', 'This field cannot start or end with spaces.')], verbose_name="User's name")),
                ('email', models.EmailField(db_index=True, max_length=254, verbose_name="User's email")),
                ('age', models.PositiveIntegerField(db_index=True, validators=[django.core.validators.MaxValueValidator(limit_value=150, message='A person can not be so old')], verbose_name="User's age")),
            ],
        ),
    ]
