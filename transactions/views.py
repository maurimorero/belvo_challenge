from django.shortcuts import render
from django.http import JsonResponse, HttpResponseBadRequest
from rest_framework import permissions, status, viewsets
from rest_framework.parsers import JSONParser
from transactions.models.transaction import Transaction
from transactions.models.transaction_user import TransactionUser
from transactions.serializers import TransactionUserSerializer, TransactionSerializer, UserSummarySerializer, \
    UserCategorySummarySerializer
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
import logging

logger = logging.getLogger(__name__)


# Create your views here.
class TransactionUserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows TransactionUser to be viewed, edited or created.
    """
    queryset = TransactionUser.objects.all()
    serializer_class = TransactionUserSerializer
    permission_classes = [permissions.IsAuthenticated]

    def create(self, request, *args, **kwargs):
        data = JSONParser().parse(request)

        try:
            was_created, transaction_user = TransactionUser.create(data)
        except Exception as e:
            logger.error("Bad request trying to create a new transaction user")
            return JsonResponse({'error': str(e)}, status=HttpResponseBadRequest.status_code)
        else:
            message = 'Transaction user created successfully' if was_created else 'Transaction user already in DB'
            logger.info(f"{message} - ID = {transaction_user.id}")
            return JsonResponse({'message': message,
                                 'id': transaction_user.id},
                                status=200)


class TransactionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Transaction to be viewed, edited or created.
    """
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
    permission_classes = [permissions.IsAuthenticated]

    def create(self, request, *args, **kwargs):
        data = JSONParser().parse(request)

        try:
            transactions_details = Transaction.create(data)
        except Exception as e:
            logger.error("Bad request trying to create a new transaction")
            return JsonResponse({'error': str(e)}, status=HttpResponseBadRequest.status_code)
        else:
            logger.info(f"Transactions successfully processed - Details: {transactions_details}")
            return JsonResponse({'message': 'Transactions successfully processed',
                                 'details': transactions_details},
                                status=200)


@api_view()
@permission_classes([permissions.IsAuthenticated])
def get_summary(request, id):
    try:
        transaction_user = TransactionUser.objects.get(id=id)
    except TransactionUser.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == "GET":
        from_date = request.GET.get("from_date", "")
        to_date = request.GET.get("to_date", "")
        serializer = UserSummarySerializer(
            transaction_user,
            context={
                "from_date": from_date,
                "to_date": to_date
            }
        )
        return Response(serializer.data)


@api_view()
@permission_classes([permissions.IsAuthenticated])
def get_category_summary(request, id):
    try:
        transaction_user = TransactionUser.objects.get(id=id)
    except TransactionUser.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == "GET":
        serializer = UserCategorySummarySerializer(
            transaction_user
        )
        return Response(serializer.data)
