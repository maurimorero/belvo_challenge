from django.db import models, transaction
from transactions.models.transaction_user import TransactionUser
import datetime


class Transaction(models.Model):
    reference = models.CharField(
        verbose_name="Transaction's reference",
        max_length=6,
        blank=False,
        unique=True,
        db_index=True)
    account = models.CharField(
        verbose_name="Transaction's account",
        max_length=6,
        blank=False,
        db_index=True)
    date = models.DateField(verbose_name="Transaction's date")
    amount = models.DecimalField(verbose_name='Transaction amount',
                                 decimal_places=2,
                                 max_digits=20)
    transaction_type = models.CharField(verbose_name='Transaction Type',
                                        max_length=7)
    category = models.CharField(
        verbose_name="Transaction's name",
        max_length=200,
        blank=False,
        db_index=True)
    transaction_user = models.ForeignKey(TransactionUser,
                                         on_delete=models.CASCADE,
                                         db_index=True,
                                         related_name="transactions")

    def __get_transaction_type(self):
        """ This method takes care of the constraint 'There are only two types of transactions: inflow and outflow' """
        self.transaction_type = "outflow" if self.amount < 0 else "inflow"

    def save(self, *args, **kwargs):
        """ This method is overridden for making sure that transaction_type has been fixed """
        self.__get_transaction_type()
        super(Transaction, self).save(*args, **kwargs)

    @staticmethod
    @transaction.atomic
    def create(data: dict) -> 'Transaction':
        """Create a new instance of Transaction (support for bulk operations)
        Args:
            data: Is expected to receive a list of dict, where each dict is populated with transaction data
        Input Example:
            [
                {"reference": "000001",
                 "account": "ACC001",
                 "date": "2020-05-30",
                 "amount": -100,
                 "transaction_type": "outflow",
                 "category": "taxes",
                 "transaction_user": 1
                 },
                {"reference": "000002",
                 "account": "ACC001",
                 "date": "2020-09-08",
                 "amount": 200,
                 "transaction_type": "inflow",
                 "category": "salary",
                 "transaction_user": 1
                 }
            ]
        Returns:
            dict: summary of new transactions created and those ones that has been skipped
        """
        transactions_references_stored = []
        transactions_references_skipped = []
        for transaction in data:
            transaction_user = TransactionUser.objects.get(id=transaction.get('transaction_user'))
            transaction_info = {'reference': transaction.get('reference'),
                                'account': transaction.get('account'),
                                'amount': transaction.get('amount'),
                                'transaction_type': transaction.get('transaction_type'),
                                'category': transaction.get('category'),
                                'date': datetime.datetime(*[int(item) for item in transaction.get('date').split('-')])}
            try:
                transaction_type = "outflow" if float(transaction.get('amount')) < 0 else "inflow"
                transaction_obj, was_created = Transaction.objects.get_or_create(
                    reference=transaction_info['reference'],
                    account=transaction_info['account'],
                    date=transaction_info['date'],
                    amount=transaction.get('amount'),
                    transaction_type=transaction_type,
                    category=transaction_info['category'],
                    transaction_user=transaction_user, )

                transactions_references_stored.append(transaction.get('reference')) if was_created \
                    else transactions_references_skipped.append(transaction.get('reference'))
            except Exception as e:
                transactions_references_skipped.append(transaction.get('reference'))
                continue

        return {'stored': transactions_references_stored,
                'skipped': transactions_references_skipped}
