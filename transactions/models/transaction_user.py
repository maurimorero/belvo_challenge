from django.db import models, transaction
from django.core.validators import MaxValueValidator, RegexValidator
import datetime
import decimal


class TransactionUser(models.Model):
    name = models.CharField(
        verbose_name="User's name",
        max_length=200,
        blank=False,
        db_index=True)
    email = models.EmailField(verbose_name="User's email",
                              unique=True,
                              db_index=True)
    age = models.PositiveIntegerField(verbose_name="User's age")

    @staticmethod
    @transaction.atomic
    def create(data: dict) -> 'TransactionUser':
        """Create a new instance of TransactionUser

        Args:
            data (dict): TransactionUser dictionary data
        Returns:
            (bool, TransactionUser): was_created flag, user created
        """
        transaction_user_info = {
            'name': data.get('name'),
            'email': data.get('email'),
            'age': data.get('age'),
        }
        transaction_user, was_created = TransactionUser.objects.get_or_create(name=transaction_user_info['name'],
                                                                              email=transaction_user_info['email'],
                                                                              age=transaction_user_info['age'])
        return was_created, transaction_user

    def get_summary(self, from_date, to_date):
        """
        This method prepares a summary of every account of the related user
        Args:
            from_date (str): Date range from
            to_date (str): Date range to
        Returns:
            list: list populated with dicts, one dict for each account
        Example:
            {
                "summary": [
                    {
                        "account": "ACC001",
                        "total_outflow": -150.0,
                        "total_inflow": 250.0,
                        "balance": 100.0
                    },
                    {
                        "account": "ACC002",
                        "total_outflow": -300.0,
                        "total_inflow": 0.0,
                        "balance": -300.0
                    }
                ]
            }
        """
        # TODO: This method performance can be improved with only one DB query
        try:
            from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d").date()
            to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d").date()
        except:
            from_date = datetime.date(1900, 1, 1)
            to_date = datetime.date(9999, 12, 31)

        total_inflow = list((self.transactions.filter(date__range=[from_date, to_date],
                                                      transaction_type="inflow").values("account")
                             .annotate(total_inflow=models.Sum('amount'))).order_by("account"))
        total_outflow = list((self.transactions.filter(date__range=[from_date, to_date],
                                                       transaction_type="outflow").values("account")
                              .annotate(total_outflow=models.Sum('amount'))).order_by("account"))

        # Collect aux information for data merge (Since one list is longer than the other, we need to iterate through
        # the longest one)
        list_to_iterate = {'list': total_inflow if len(total_inflow) >= len(total_outflow) else total_outflow,
                           'type': 'total_inflow' if len(total_inflow) >= len(total_outflow) else 'total_outflow'}

        other_list = {'list': total_inflow if list_to_iterate['type'] == 'total_outflow' else total_outflow,
                      'type': 'total_inflow' if list_to_iterate['type'] == 'total_outflow' else 'total_outflow'}
        report = []
        for idx, account in enumerate(list_to_iterate['list']):
            opositive = next((item for item in other_list['list'] if item["account"] == account['account']), None)
            if opositive:
                # Merge accounts details
                account_summary = {**account, **opositive}
            else:
                account_summary = account.copy()
                # Complete missing values
                account_summary[other_list['type']] = decimal.Decimal(0.0)
            # Balance calculation
            account_summary['balance'] = account_summary['total_inflow'] + account_summary['total_outflow']
            report.append(account_summary)
        return report

    def get_category_summary(self):
        """
        This method prepares a user's summary by category that shows the sum of amounts per transaction category
        Returns:
            dict: summary splited in "inflow" and "outflow" category types
        Example:
                {
                    "category_summary": {
                        "inflow": {
                            "salary": 250.0
                        },
                        "outflow": {
                            "taxes": -100.0,
                            "groceries": -200.0,
                            "bank fees": -150.0
                        }
                    }
                }
        """
        # DB queries
        inflow_summary = list((self.transactions.filter(transaction_type="inflow").values("category")
                               .annotate(summary=models.Sum('amount'))))
        outflow_summary = list((self.transactions.filter(transaction_type="outflow").values("category")
                                .annotate(summary=models.Sum('amount'))))
        # response preparation
        response = {'inflow': {}, 'outflow': {}}
        for category in inflow_summary:
            response['inflow'][category['category']] = category['summary']
        for category in outflow_summary:
            response['outflow'][category['category']] = category['summary']
        return response
