from transactions.models.transaction_user import TransactionUser
from transactions.models.transaction import Transaction
from rest_framework import serializers


class TransactionUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = TransactionUser
        fields = ['id', 'name', 'email', 'age', ]


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ['id', 'reference', 'account', 'date', 'amount', 'transaction_type', 'category', 'transaction_user', ]


class UserSummarySerializer(serializers.ModelSerializer):
    summary = serializers.SerializerMethodField()

    def get_summary(self, instance):
        from_date = self.context.get("from_date")
        to_date = self.context.get("to_date")
        return instance.get_summary(from_date, to_date)

    class Meta:
        model = TransactionUser
        fields = ["summary"]


class UserCategorySummarySerializer(serializers.ModelSerializer):
    category_summary = serializers.SerializerMethodField()

    def get_category_summary(self, instance):
        return instance.get_category_summary()

    class Meta:
        model = TransactionUser
        fields = ["category_summary"]
