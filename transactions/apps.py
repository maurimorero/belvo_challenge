from django.apps import AppConfig


class TransactionsConfig(AppConfig):
    name = 'transactions'
    label = 'Transactions'
    verbose_name = 'Transactions'

