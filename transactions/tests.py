from django.test import TestCase
from django.contrib.auth.models import User
from transactions.models.transaction_user import TransactionUser
from transactions.serializers import TransactionUserSerializer, TransactionSerializer
from transactions.models.transaction import Transaction
from django.db.utils import IntegrityError
from django.db import transaction
from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status


# Information for testing purpose
TRANSACTION_USERS_INFO = [
    {'name': 'Mauricio Morero',
     'age': 35,
     'email': 'maurimorero@gmail.com'
     },
    {'name': 'Lionel Messi',
     'age': 34,
     'email': 'lio@barcelona.com'
     },
    {'name': 'Cristiano Ronaldo',
     'age': 35,
     'email': 'cr7@juventus.com'
     }
]

INVALID_USER_INFO = {
    'age': 35,
    'email': 'cr7@juventus.com'
    }

TRANSACTIONS_INFO = [
    {'reference': '000001',
     'account': 'ACC001',
     'date': '2020-05-30',
     'amount': -100,
     'transaction_type': 'outflow',
     'category': 'taxes',
     'transaction_user': None,
     },
    {'reference': '000002',
     'account': 'ACC001',
     'date': '2020-09-08',
     'amount': 200,
     'transaction_type': 'inflow',
     'category': 'salary',
     'transaction_user': None,
     },
    {'reference': '000003',
     'account': 'ACC001',
     'date': '2020-11-30',
     'amount': 50,
     'transaction_type': 'inflow',
     'category': 'salary',
     'transaction_user': None,
     },
    {'reference': '000004',
     'account': 'ACC001',
     'date': '2020-12-03',
     'amount': -50,
     'transaction_type': 'outflow',
     'category': 'groceries',
     'transaction_user': None,
     },
    {'reference': '000005',
     'account': 'ACC002',
     'date': '2021-02-03',
     'amount': -150,
     'transaction_type': 'outflow',
     'category': 'groceries',
     'transaction_user': None,
     },
    {'reference': '000006',
     'account': 'ACC002',
     'date': '2021-02-03',
     'amount': -150,
     'transaction_type': 'expense',
     'category': 'bank fees',
     'transaction_user': None,
     }
]

INVALID_TRANSACTION = {
    'account': 'ACC001',
    'date': '2020-05-30',
    'amount': -100,
    'transaction_type': 'outflow',
    'category': 'taxes',
    'transaction_user': 1,
    }


# Model tests
class TransactionUserTestCase(TestCase):
    """ TransactionUser model tests """

    def test_create_users(self):
        self.assertIs(len(TransactionUser.objects.all()), 0)
        for user in TRANSACTION_USERS_INFO:
            TransactionUser.objects.create(name=user['name'],
                                           age=user['age'],
                                           email=user['email'])
        self.assertIs(len(TransactionUser.objects.all()), 3)


class TransactionTestCase(TestCase):
    """ Transaction model tests """

    def test_create_transactions(self):
        user = TransactionUser.objects.create(name=TRANSACTION_USERS_INFO[0]['name'],
                                              age=TRANSACTION_USERS_INFO[0]['age'],
                                              email=TRANSACTION_USERS_INFO[0]['email'])
        self.assertIs(len(Transaction.objects.all()), 0)
        for transaction in TRANSACTIONS_INFO:
            Transaction.objects.create(
                reference=transaction['reference'],
                account=transaction['account'],
                date=transaction['date'],
                amount=transaction['amount'],
                transaction_type=transaction['transaction_type'],
                category=transaction['category'],
                transaction_user=user,
            )
        self.assertIs(len(Transaction.objects.all()), 6)

        # Validate that category 'expense' has been updated to outflow
        fixed_transaction = Transaction.objects.get(reference='000006')
        self.assertEquals(fixed_transaction.transaction_type, 'outflow')


class UserReportsTest(TestCase):
    """ User reports model tests """
    def setUp(self):
        self.transaction_user = TransactionUser.objects.create(name=TRANSACTION_USERS_INFO[0]['name'],
                                                               age=TRANSACTION_USERS_INFO[0]['age'],
                                                               email=TRANSACTION_USERS_INFO[0]['email'])
        self.transactions = []
        for transaction in TRANSACTIONS_INFO:
            Transaction.objects.create(
                reference=transaction['reference'],
                account=transaction['account'],
                date=transaction['date'],
                amount=transaction['amount'],
                transaction_type=transaction['transaction_type'],
                category=transaction['category'],
                transaction_user=self.transaction_user,
            )

    def test_get_user_summary_all_transactions(self):
        user_account_summary = self.transaction_user.get_summary("1000-01-01",
                                                                 "9999-31-12")
        account = next(ac for ac in user_account_summary if ac["account"] == 'ACC001')
        self.assertEquals(100, account['balance'])

    def test_get_user_summary_with_date_range(self):
        user_account_summary = self.transaction_user.get_summary("2020-11-01",
                                                                 "2021-02-05")
        account = next(ac for ac in user_account_summary if ac["account"] == 'ACC001')
        self.assertEquals(0.0, account['balance'])

    def test_get_category_summary(self):
        category_summary = self.transaction_user.get_category_summary()
        self.assertEquals(-200.0, category_summary['outflow']['groceries'])


# Api Validations
class UsersApiTest(TestCase):
    """ Users Api tests """
    def setUp(self):
        self.user = User.objects.create_user("admin," "admin@admin.com", "some_pass")
        self.api_client = Client()
        self.api_client.force_login(user=self.user)
        for user in TRANSACTION_USERS_INFO:
            TransactionUser.objects.create(name=user['name'],
                                           age=user['age'],
                                           email=user['email'])

    def test_get_users(self):
        response = self.api_client.get("/api/transaction_users/")
        users = TransactionUser.objects.all()
        serializer = TransactionUserSerializer(users, many=True)
        self.assertEqual(response.data, serializer.data)

    def test_user(self):
        user = list(TransactionUser.objects.filter(name="Mauricio Morero"))[0]
        response = self.api_client.get(f"/api/transaction_users/{user.id}/")
        serializer = TransactionUserSerializer(user)
        self.assertEqual(response.data, serializer.data)

    def test_invalid_user_creation(self):
        response = self.api_client.post("/api/transaction_users/",
                                        data=INVALID_USER_INFO,
                                        format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TransactionsApiTest(TestCase):
    """ Transactions Api tests """
    def setUp(self):
        self.user = User.objects.create_user("admin," "admin@admin.com", "some_pass")
        self.api_client = Client()
        self.api_client.force_login(user=self.user)
        self.transaction_user = TransactionUser.objects.create(name=TRANSACTION_USERS_INFO[0]['name'],
                                                               age=TRANSACTION_USERS_INFO[0]['age'],
                                                               email=TRANSACTION_USERS_INFO[0]['email'])
        for transaction in TRANSACTIONS_INFO:
            Transaction.objects.create(
                reference=transaction['reference'],
                account=transaction['account'],
                date=transaction['date'],
                amount=transaction['amount'],
                transaction_type=transaction['transaction_type'],
                category=transaction['category'],
                transaction_user=self.transaction_user,
            )

    def test_get_transactions(self):
        response = self.api_client.get("/api/transactions/")
        transactions = Transaction.objects.all()
        serializer = TransactionSerializer(transactions, many=True)
        self.assertEqual(response.data, serializer.data)

    def test_transaction(self):
        transaction = list(Transaction.objects.filter(reference="000004"))[0]
        response = self.api_client.get(f"/api/transactions/{transaction.id}/")
        serializer = TransactionSerializer(transaction)
        self.assertEqual(response.data, serializer.data)

    def test_invalid_transaction_creation(self):
        response = self.api_client.post("/api/transactions/",
                                        data=INVALID_TRANSACTION,
                                        format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class BadAuthApiTest(TestCase):
    """ Bad authentication Api tests """
    def setUp(self):
        self.api_client = Client()
        for user in TRANSACTION_USERS_INFO:
            TransactionUser.objects.create(name=user['name'],
                                           age=user['age'],
                                           email=user['email'])

    def test_get_users(self):
        response = self.api_client.get("/api/transaction_users/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_user(self):
        response = self.api_client.get(f"/api/transaction_users/1/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_transactions(self):
        response = self.api_client.get(f"/api/transactions/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_transaction(self):
        response = self.api_client.get(f"/api/transactions/1/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_summary(self):
        response = self.api_client.get(f"/api/transaction_users/1/summary")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ReportsApiTest(TestCase):
    """ Reports Api tests """
    def setUp(self):
        self.user = User.objects.create_user("admin," "admin@admin.com", "some_pass")
        self.api_client = Client()
        self.api_client.force_login(user=self.user)
        self.transaction_user = TransactionUser.objects.create(name=TRANSACTION_USERS_INFO[0]['name'],
                                                               age=TRANSACTION_USERS_INFO[0]['age'],
                                                               email=TRANSACTION_USERS_INFO[0]['email'])
        for transaction in TRANSACTIONS_INFO:
            Transaction.objects.create(
                reference=transaction['reference'],
                account=transaction['account'],
                date=transaction['date'],
                amount=transaction['amount'],
                transaction_type=transaction['transaction_type'],
                category=transaction['category'],
                transaction_user=self.transaction_user,
            )

    def test_summary(self):
        response = self.api_client.get(f"/api/transaction_users/{self.transaction_user.id}/summary")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['summary'][0]['balance'], 100)

    def test_category_summary(self):
        response = self.api_client.get(f"/api/transaction_users/{self.transaction_user.id}/category_summary")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['category_summary']['inflow']['salary'], 250)
