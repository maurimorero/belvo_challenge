# Belvo Challenge

App with a simple API (REST API) that allows to register users' transactions and have an overview of how they 
are using their money.

## Requirements
- docker / docker-compose
- python3.7 / pip3
- virtualenv

## How to run

### Development

1. Clone repository and change directory to repository dir.
2. Create virtualenv: `mkvirtualenv ~/.virtualenvs/belvo_challenge --python=/usr/bin/python3.7` and activate them: 
   `source ~/.virtualenvs/belvo_challenge/bin/activate`
3. Install requirements `pip3 install -r requirements.txt`
4. Run `docker-compose up -d` to start the Django development webserver
5. Run database migrations: `docker-compose exec web python manage.py migrate`
6. If first time running, create a superuser: `docker-compose exec web python manage.py createsuperuser` (This superuser
   can be used for authentication in api usage)

## API List

Authentication: For using any of these endpoints, you have to provide credentials (Basic Auth) in authorization headers.
You can use the superuser created in step  #6 in "How to run" section

- [GET] /api/transaction_users/ ====> _Retrieves all users_

- [POST] /api/transaction_users/ ====> _For creating a new user._
Payload example:
  ```json
  {
    "name": "Mauricio Morero",
    "email": "maurimorero@gmail.com",
    "age": 35
   }
    ```

- [GET] /api/transaction_users/**USER_ID**/ ====> _Retrieves user's details_

- [GET] /api/transactions/ ====> _Retrieves all transactions in the system_

- [POST] /api/transactions/ ====> _Endpoint for creating new transactions._
Payload example:
  ```json
  [
    {"reference": "003001",
     "account": "ACC001",
     "date": "2020-05-30",
     "amount": -100,
     "transaction_type": "outflow",
     "category": "taxes",
     "transaction_user": 1
     },
    {"reference": "000006",
     "account": "ACC002",
     "date": "2021-02-03",
     "amount": -150,
     "transaction_type": "expense",
     "category": "bank fees",
     "transaction_user": 1
     }
    ]
    ```
  
- [GET] /api/transactions/**TRANSACTION_ID**/ ====> _Retrieves transaction details_

- [GET] /api/transaction_users/**USER_ID**/summary?from_date=**FROM_DATE**&to_date=**TO_DATE** ====> _User's summary by 
  account that shows the balance of the account, total inflow and total outflows. It is possible to specify a date range_
  
  Note: Date format is "YYYY-mm-dd", if the date is provided in any other format, the query will include all 
  transactions 

- [GET] /api/transaction_users/**USER_ID**/category_summary ====> _User's summary by category that shows the sum of 
  amounts per transaction category_

## Credits

Mauricio Blas Morero

(maurimorero@gmail.com)

https://www.linkedin.com/in/mauricio-morero-a19a66a/