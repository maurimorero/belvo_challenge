# pull official base image
FROM python:3.7.9-alpine

# set work directory
WORKDIR /usr/src/belvo_challenge

# set environment variables

# TODO: check this both values for performance impact
# Prevents Python from writing pyc files to disc (equivalent to python -B option)
ENV PYTHONDONTWRITEBYTECODE 1
# Prevents Python from buffering stdout and stderr (equivalent to python -u option)
ENV PYTHONUNBUFFERED 1
#

# install psycopg2 dependencies
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev

# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt .
RUN pip install -r requirements.txt

# copy entrypoint.sh
COPY entrypoint.sh .

# copy project
COPY ./ .

# run entrypoint.sh
ENTRYPOINT ["/usr/src/belvo_challenge/entrypoint.sh"]

