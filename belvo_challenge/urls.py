"""aut_hub URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf.urls import url
from rest_framework import routers
from transactions import views

admin.site.site_header = 'Transactions'
admin.site.site_title = 'Transactions'
admin.site.index_title = 'Transactions'

router = routers.DefaultRouter()
router.register(r'transaction_users', views.TransactionUserViewSet)
router.register(r'transactions', views.TransactionViewSet)

urlpatterns = [
    path('', admin.site.urls),
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/transaction_users/(?P<id>[0-9]+)/summary$', views.get_summary),
    url(r'^api/transaction_users/(?P<id>[0-9]+)/category_summary$', views.get_category_summary)
]
